const { Sequelize, DataTypes } = require('sequelize');
const config = require('../../config');

const db = new Sequelize(
  config.DB_NAME,
  config.DB_USERNAME,
  config.DB_PASSWORD,
  {
    host: config.DB_HOST,
    port: config.DB_PORT,
    dialect: config.DB_DIALECT,
    logging: false
  }
);

let user = db.define('user', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
  name: {type: DataTypes.STRING(50), allowNull: false},
  email: {type: DataTypes.STRING(50), allowNull: false},
  phone: {type: DataTypes.STRING(15), allowNull: false, unique: true},
  password: {type: DataTypes.STRING(255), allowNull: false}
}, {
  freezeTableName: true
})

module.exports = {
  user
}